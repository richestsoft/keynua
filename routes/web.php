<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/', ['as' => '/', 'uses' => 'HomeController@index']);

Auth::routes();

Route::get('home', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('products', ['as' => 'products', 'uses' => 'ProductController@index']);
Route::get('upload', ['as' => 'upload', 'uses' => 'UploadController@index']);
Route::get('contact', ['as' => 'contact', 'uses' => 'ContactController@index']);
Route::post('contactus_query', ['as' => 'contactus', 'uses' => 'ContactController@submitQuery']);
Route::get('/setting', function () {
    return view('setting');
});

// User related routes
Route::get('authy/status', 'Auth\AuthyController@status');
Route::post('authy/callback', ['middleware' => 'validate_authy', 'uses'=>'Auth\AuthyController@callback']);

Route::resource('auth', 'Auth\AuthController');
Route::resource('password', 'Auth\PasswordController');
