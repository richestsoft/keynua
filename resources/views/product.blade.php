@extends('layouts.app')

@section('htmlheader_title')
	Products
@endsection

@section('content')
   <div class="product_main">
        <div class="container">
        <h1 class="head-main"><span>Products</span></h1>
        <h2 class="contact_head">Welcome to the Keynua API</h2>
        <p>Keynua is your payments partner, not just a payments platform,Products. We create seamless e-commerce, Already Managed experiences that maximize opportunities for companies across the globe, from small businesses to large enterprises.</p>
        <h2 class="contact_head">Pick the right Product for your project</h2>
        <div class="products">
            <div class="row">
                <div class="col-sm-6">
                    <div class="product_buy">
                        <div class="product_img"><img src="{{ asset('/images/product.png') }}"></div>
                        <h1>Buy Sell Digital Currency</h1>
                        <p>identify which email addresses are deliverable and undeliverable.upload an email lists from your computer or import from your ESP</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="product_buy">
                        <div class="product_img"><img src="{{ asset('/images/product.png') }}"></div>
                        <h1>GDAX</h1>
                        <p>identify which email addresses are deliverable and undeliverable.upload an email lists from your computer or import from your ESP</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="product_buy">
                        <div class="product_img"><img src="{{ asset('/images/product.png') }}"></div>
                        <h1>Developer Platform</h1>
                        <p>identify which email addresses are deliverable and undeliverable.upload an email lists from your computer or import from your ESP</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="product_buy">
                        <div class="product_img"><img src="{{ asset('/images/product.png') }}"></div>
                        <h1>Merchant Tools</h1>
                        <p>identify which email addresses are deliverable and undeliverable.upload an email lists from your computer or import from your ESP</p>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
