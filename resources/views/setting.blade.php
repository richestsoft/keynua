@extends('layouts.app')

@section('htmlheader_title')
	Upload
@endsection

@section('content')
<div class="product_main">
        <div class="container">
        <h1 class="head-main"><span>Account Setting</span></h1>
        <div class="Account_setting">
            <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Profile</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Security</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Preferences</a></li>
    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Developers Options</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="home">
        <div class="tab_form">
          <form action="">
            <div class="row">
                <div class="col-sm-6">
                    <label>Name</label>
                    <input type="text" name="">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Email Address</label>
                    <input type="email" name="">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Time Zone</label>
                    <div class="select">
                        <select>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <input type="submit" value="Deactivate Account" name="">
                </div>
            </div>

          </form>  
        </div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="profile">
        <div class="tab_form">
          <form action="">
            <div class="row">
                <div class="col-sm-6">
                    <label>Name</label>
                    <input type="text" name="">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Email Address</label>
                    <input type="email" name="">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Time Zone</label>
                    <div class="select">
                        <select>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <input type="submit" value="Deactivate Account" name="">
                </div>
            </div>

          </form>  
        </div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="messages">
        <div class="tab_form">
          <form action="">
            <div class="row">
                <div class="col-sm-6">
                    <label>Name</label>
                    <input type="text" name="">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Email Address</label>
                    <input type="email" name="">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Time Zone</label>
                    <div class="select">
                        <select>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <input type="submit" value="Deactivate Account" name="">
                </div>
            </div>

          </form>  
        </div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="settings">
        <div class="tab_form">
          <form action="">
            <div class="row">
                <div class="col-sm-6">
                    <label>Name</label>
                    <input type="text" name="">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Email Address</label>
                    <input type="email" name="">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Time Zone</label>
                    <div class="select">
                        <select>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                            <option>12pm</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <input type="submit" value="Deactivate Account" name="">
                </div>
            </div>

          </form>  
        </div>
    </div>
  </div>

        </div>
        
        </div>
    </div>
@endsection
