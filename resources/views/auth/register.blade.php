@extends('layouts.app-noheader')

@section('htmlheader_title')
	Register
@endsection

@section('single_css')
  <link href="https://www.authy.com/form.authy.min.css" media="screen" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="sign_main">
        <div class="container">
        <h1 class="head-main">Sign Up To <span>Keynua</span></h1>
        <h2 class="contact_head">Start Something New <span>With Keynua</span></h2>
                <div class="sign_inner">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-sm-6"><input type="text" class="form-control" name="name" placeholder="First Name*" value="{{ old('name') }}"></div>
                            <div class="col-sm-6"><input type="text" name="" placeholder="Last Name*"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6"><select id="authy-countries" name="country_code"></select></div>
                            <div class="col-sm-6"><input type="phone_number" class="form-control" name="phone_number" id="authy-cellphone" placeholder="Enter Phone Number *"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12"><input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Addresses*"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12"><input type="password" class="form-control" name="password" placeholder="Enter Your Password*"></div>
                        </div>
						<div class="row">
                            <div class="col-sm-12"><input type="password" class="form-control" name="password_confirmation"placeholder="Confirm Password*" ></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 clear_form"><input type="submit" value="clear" name=""></div>
                            <div class="col-sm-6"><button type="submit">Submit</button></div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
@endsection

@section('single_scripts')

  <script src="https://www.authy.com/form.authy.min.js" type="text/javascript"></script>
  <script>
    Authy.UI.ui = new Authy.UI();
    Authy.UI.ui.init();
    $('.countries-input').addClass('form-control');
  </script>
@endsection
