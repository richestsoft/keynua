@extends('layouts.app-noheader')

@section('htmlheader_title')
	Login
@endsection

@section('modals')
<div class="modal fade" id="authy-modal">
  <div class='modal-dialog'>
    <div class="modal-content">
      <div class='modal-header'>
        <h4 class='modal-title'>Please Authenticate</h4>
      </div>
      <div class='modal-body auth-ot'>
        <div class='help-block'>
          <i class="fa fa-spinner fa-pulse"></i> Waiting for OneTouch Approval ...
        </div>
      </div>
      <div class='modal-body auth-token'>
        <div class='help-block'>
          <i class="fa fa-mobile"></i> Authy OneTouch not available
        </div>
        <p>Please enter your Token</p>
        <form id="authy-sms-form" class="form-horizontal" role="form" method="POST" action="/auth/twofactor">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class='form-group'>
            <label class="col-md-4 control-label" for="token">Authy Token</label>
            <div class='col-md-6'>
              <input type="text" name="token" id="authy-token" ng-model="token" value="" class="form-control" autocomplete="off" />
            </div>
          </div>
          <a value="Verify" class="btn btn-default" href="#" ng-click="cancel()">Cancel</a>
          <input type="submit" name="commit" value="Verify2" class="btn btn-success" ng-click="verifyToken(token)" />
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('content')
	<div class="sign_main sign_in_main">
		@if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Whoops!</strong> There were some problems with your input.<br><br>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
        <div class="container">
			<h1 class="head-main">Sign In To <span>Keynua</span></h1>
                <div class="sign_inner">
                    <form id="login-form" class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-sm-12"><input type="email" name="email" placeholder="Email Addresses"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12"><input type="password" name="password" placeholder="Enter Your Password"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10 checkbox_form">
                                <input type="checkbox" name="remember"><span>Remember me signed in on this Computer</span>
                            </div>
                            <div class="col-sm-2 tip_form"><i class="fa fa-info-circle" aria-hidden="true"></i></div>
                        </div>
						<div class="row">
                            <div class="col-sm-12 checkbox_form"><a href="{{ url('/password/email') }}">Forgot Your Password?</a></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 clear_form"><input type="submit" value="clear" name=""></div>
                            <div class="col-sm-6"><input type="submit" name="submit" value="Login" ></div>
                        </div>
                    </form>
                </div>
                <div class="crate_acc">
                    <h4>Create New Account <a href="{{ url('/register') }}">Sign Up</a></h4>
                </div>
        </div>
    </div>
@endsection

@section('single_scripts')
<script src="{{ url('/js/sessions.js') }}" type="text/javascript"></script>
@endsection
