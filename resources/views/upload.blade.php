@extends('layouts.app')

@section('htmlheader_title')
	Upload
@endsection

@section('content')
 <div class="sign_main">
        <div class="container">
        <h1 class="head-main">Project <span>Upload</span></h1>
                <div class="sign_inner">
                    <form action="">
                        <div class="row">
                            <div class="col-sm-12"><input type="text" name="" placeholder="Project Name"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12"><input type="email" name="" placeholder="Email Address"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txtarea">
                                <textarea></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 upload_project">
                                <input type="text" name="">
                                <div class="upload_file"><input type="file" name="">
                                <span>Upload</span></div>
                            </div>
                        </div>
                        
                        <div class="upload_progress">
                            <div class="row">
                                <div class="col-sm-6"><h4>Wallet Details .csv</h4></div>
                                <div class="col-sm-6">
                                    <div class="progress">
                                      <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        <span class="sr-only">60% Complete</span>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">    
                                <div class="col-sm-6"><h4>Product Invoice 2017.XLS</h4></div>
                                <div class="col-sm-6">
                                    <div class="progress">
                                      <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        <span class="sr-only">60% Complete</span>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6 upload-submit">
                                <input type="submit" value="submit" name="">
                            </div>
                            <div class="col-sm-6 form-upload">
                                <span>Sucessfully Submitted  !</span>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
@endsection
