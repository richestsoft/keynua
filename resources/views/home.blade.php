@extends('layouts.app')

@section('htmlheader_title')
	Home
@endsection

@section('content')
    <div class="banner">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-7 banner-left">
    				<div class="banner-text">
    					<h1>Powering More<br> Than Just Payments </h1>
    					<p>Keynua is your payments partner, not just a payments platform,Products. We create seamless e-commerce, Already Managed experiences that maximize opportunities for companies across the globe, from small businesses to large enterprises.</p>
    					<div class="banner-sign">
    						<a href="#" class="sign">Sign Up</a>
    						<a href="#" class="try">Try us</a>
    					</div>
    				</div>
    			</div>
    			<div class="col-sm-5 banner-right">
    				<div class="banner_img">
    					<img src="{{ asset('/images/banner_img.png') }}">
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div class="start">
    	<div class="container">
    		<h1 class="head-main">Start Something New <span>With Keynua</span></h1>
    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
    		</p>
    		<div class="row">
    			<div class="col-sm-4">
    				<div class="start_section">
    				<div class="start_img">
    					<img class="img-dtl" src="{{ asset('/images/start1.png') }}">
    					<img class="img-dtl-hover" src="{{ asset('/images/start1-hover.png') }}">
    				</div>
    					<div class="srart_detail categry1">
		    				<a href="#"><span>Eliminate </span><br>Chargebacks and Fraud</a>
		    				<h4>Suspendisse velit ticol sodales, viverra sigirton vitae, accumsan orci mauris</h4>
		    			</div>
    				</div>
    			</div>
    			<div class="col-sm-4">
    				<div class="start_section">
    				<div class="start_img">
    					<img class="img-dtl" src="{{ asset('/images/start2.png') }}">
    					<img class="img-dtl-hover" src="{{ asset('/images/start2-hover.png') }}">
    				</div>
    					<div class="srart_detail categry2">
		    				<a href="#"><span>Get </span>Paid Quickly</a>
		    				<h4>Suspendisse velit ticol sodales, viverra sigirton vitae, accumsan orci mauris</h4>
		    			</div>
    				</div>
    			</div>
    			<div class="col-sm-4">
    				<div class="start_section">
    				<div class="start_img">
    					<img class="img-dtl" src="{{ asset('/images/start3.png') }}">
    					<img class="img-dtl-hover" src="{{ asset('/images/start3-hover.png') }}">
    				</div>
    					<div class="srart_detail categry3">
		    				<a href="#"><span>Issue </span>Refunds</a>
		    				<h4>Suspendisse velit ticol sodales, viverra sigirton vitae, accumsan orci mauris</h4>
		    			</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div class="business_section">
    	<div class="container">
    		<h1 class="head-main"><span>Trusted by over</span> 45,000 Businesses</h1>
    		<h4>Merchants can accept bitcoin payments worldwide</h4>
    		<ul>
    			<li><img src="{{ asset('/images/business1.jpg') }}"></li>
    			<li><img src="{{ asset('/images/business2.jpg') }}"></li>
    			<li><img src="{{ asset('/images/business3.jpg') }}"></li>
    			<li><img src="{{ asset('/images/business4.jpg') }}"></li>
    			<li><img src="{{ asset('/images/business5.jpg') }}"></li>
    			<li><img src="{{ asset('/images/business6.jpg') }}"></li>
    		</ul>
    		<a href="#">View More</a>
    	</div>
    </div>
    <div class="payment">
      <div class="payment-inner clearfix">
      <div class="payment_detail_rs">
      <h1 class="head-main">Keynua Wallet <span>Payments</span></h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
      </div>
    	<div class="col-sm-6 apply-left">
	         <div class="col-sm-9">
	            <h1 class="title-online">Lorem ipsum dolor sit</h1>
	            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt  </p>
	         </div>
	         <div class="col-sm-3">  <span class="number">1</span></div>
	    </div>
	      <div class="col-sm-6 apply-right">
	         <div class="col-sm-3">  <span class="number">2</span></div>
	         <div class="col-sm-9">
	            <h1 class="title-online">Lorem ipsum dolor sit</h1>
	            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt  </p>
	         </div>
	      </div>
	      <div class="col-sm-6 apply-left">
	         <div class="col-sm-9">
	            <h1 class="title-online">Lorem ipsum dolor sit</h1>
	            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt  </p>
	         </div>
	         <div class="col-sm-3">  <span class="number">3</span></div>
	      </div>
	      <div class="col-sm-6 apply-right">
	         <div class="col-sm-3">  <span class="number">4</span></div>
	         <div class="col-sm-9">
	            <h1 class="title-online">Lorem ipsum dolor sit</h1>
	            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt  </p>
	         </div>
	      </div>
	      <div class="col-sm-6 apply-left">
	         <div class="col-sm-9">
	            <h1 class="title-online">Lorem ipsum dolor sit</h1>
	            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt  </p>
	         </div>
	         <div class="col-sm-3">  <span class="number">5</span></div>
	      </div>
	      <div class="col-sm-6 apply-right">
	         <div class="col-sm-3">  <span class="number">6</span></div>
	         <div class="col-sm-9">
	            <h1 class="title-online">Lorem ipsum dolor sit</h1>
	            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt  </p>
	         </div>
	      </div>
	    </div>
    </div>
    <div class="pay_wallet">
    	<div class="container">
    		<h1>Keynua Wallet <span>Payments</span></h1>
    		<a href="#">CONTACT US</a>
    	</div>
    </div>
@endsection
