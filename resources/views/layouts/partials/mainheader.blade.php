<!-- Main Header -->
	@if((Route::is('home')) || (Route::is('/')))
		@php
		 $header = 'header-home'
		@endphp
		  
	@else 
		@php
		 $header = 'header-default'
		@endphp
	@endif
    <div class="{{ $header }}">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 logo">
					<a href="{{ route('home') }}"><img src="{{ asset('/images/logo.png') }}"></a>
				</div>
				<div class="col-sm-8 navigation">
					<nav class="navbar navbar-default">
						  <div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							  <span class="sr-only">Toggle navigation</span>
							  <span class="icon-bar"></span>
							  <span class="icon-bar"></span>
							  <span class="icon-bar"></span>
							</button>
						  </div>
						  <div id="navbar" class="navbar-collapse collapse">
							<ul class="nav navbar-nav">
							  <li class="{{ isActiveRoute('/') }}"><a href="{{ route('/') }}">Home</a></li>
							  <li class="dropdown">
								<a href="{{ route('products') }}" class="dropdown-toggle" data-toggle="dropdown">Products<b class="caret"></b></a>                      

								<ul class="dropdown-menu mega-menu">
			
									<li class="mega-menu-column">
										<a class="" href="#">
											<i class="fa fa-cogs" aria-hidden="true"></i>
											<div class="mega_icon">
											  <h3 class="">Buy Sell Digital Currency</h3>
											</div>
										</a>
									</li>    
				
									<li class="mega-menu-column">
										<a class="" href="#">
											<i class="fa fa-cogs" aria-hidden="true"></i>
											<div class="mega_icon">
											  <h3 class="">GDAX</h3>
											</div>
										</a>
									</li>
									
									<li class="mega-menu-column">
										<a class="" href="#">
											<i class="fa fa-cogs" aria-hidden="true"></i>
											<div class="mega_icon">
											  <h3 class="">Developer Platform</h3>
											</div>
										</a>
									</li>   
									
									<li class="mega-menu-column">
										<a class="" href="#">
											<i class="fa fa-cogs" aria-hidden="true"></i>
											<div class="mega_icon">
											  <h3 class="">Merchant Tools</h3>
											</div>
										</a>
									</li>
								</ul><!-- dropdown-menu -->
								
							  </li><!-- /.dropdown -->
							  <li class="{{ isActiveRoute('upload') }}"><a href="{{ route('upload') }}">Upload</a></li>
							  <li class="{{ isActiveRoute('contact') }}"><a href="{{ route('contact') }}">Contact Us</a></li>
							</ul>
						  </div><!--/.nav-collapse -->
					  </nav>
					  <div class="signup_main">
						<ul>
							<li><a href="{{ url('/login') }}">Sign In</a></li>
							<li class="sign_btn"><a href="{{ url('/register') }}">Sign up</a></li>
						</ul>
					  </div>
				</div>
			</div>
		</div>
    </div>