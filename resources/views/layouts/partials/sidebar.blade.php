<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset('/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                  
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->
            <li {{ (Request::is('*home') ? 'class="active"' : '') }}><a href="{{ url('/user') }}"><i class='fa fa-link'></i> <span>Home</span></a></li>
            <li {{ (Request::is('*orderForm') ? 'class="active"' : '') }}><a href="{{ url ('/user/orderForm') }}"><i class='fa fa-link'></i> <span>Add Order</span></a></li>
		    <li {{ (Request::is('*checkOrders') ? 'class="active"' : '') }}><a href="{{ url ('/user/checkOrders') }}"><i class='fa fa-link'></i> <span>Check Orders</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
