<!-- Main Footer -->
    <div class="footer">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-5">
    			<img src="{{ asset('/images/footer-logo.png') }}">
    				<p>eusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    			</div>
    			<div class="col-sm-2 footer_link">
    				<h3>Explore</h3>
    				<ul>
    					<li><a href="#">Home</a></li>
    					<li><a href="#">Products</a></li>
    					<li><a href="#">Payment</a></li>
    					<li><a href="#">Currency</a></li>
    				</ul>
    			</div>
    			<div class="col-sm-2 footer_link">
    				<h3>Quick Links</h3>
    				<ul>
    					<li><a href="#">Help</a></li>
    					<li><a href="#">Contact Us</a></li>
    					<li><a href="#">Sign In</a></li>
    					<li><a href="#">Log In</a></li>
    				</ul>
    			</div>
    			<div class="col-sm-3 socials-rs">
    			<h3>Get in touch</h3>
    				<ul>
                        <li class="facebook"> <a href="#"> <i class="fa fa-facebook" aria-hidden="true"></i> </a></li>
                        <li class="twitter"> <a href="#"> <i class="fa fa-twitter" 
                        aria-hidden="true"></i> </a></li>
                        <li class="linkdea"> <a href="#"> <i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        <li class="pinerset"> <a href="#"> <i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                     </ul>
    			</div>
    		</div>
    	</div>
    </div>
    <div class="reserverd">
    	<div class="col-sm-12">
    		<h3>© 2017 Keynua. All Rights Reserved.</h3>
    	</div>
    </div>
