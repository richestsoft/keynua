<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<body>
	
	@yield('modals')
	<div class="header_bar">
        <div class="container">
            <a href="{{ route('/') }}"><img src="{{ asset('/images/logo.png') }}"></a>
        </div>
    </div>
    @yield('content')
	<div class="reserverd">
    	<div class="col-sm-12">
    		<h3>© 2017 Keynua. All Rights Reserved.</h3>
    	</div>
    </div>
@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
