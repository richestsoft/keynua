<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      </head>
  <body>
    <header style="background: #367fa9 none repeat scroll 0 0; color: #fff; font-size: 20px;  font-weight: 600; padding: 18px 20px; text-align: center;">
		<a href="#" style="color: #fff; text-decoration: none;">Laravel</a>
	</header>
	
	<div class="mian_content" style=" margin: 0 auto; max-width: 500px; width: 100%;">
		<h2 style="text-align:center"> Here is ContactUs Query: </h2>
		<div class="welcome_txt" style="background: #f1f1f1 none repeat scroll 0 0; font-size: 28px; padding: 30px 20px;">
			
			<ul>
			  <li>Name: <strong>{{ $name }}</strong></li>
			  <li>Email: <strong>{{ $email }}</strong></li>
			  <li>Subject: <strong>{{ $subject }}</strong></li>
			</ul>
			<hr>
			<p>
			  @foreach ($messageLines as $messageLine)
				{{ $messageLine }}<br>
			  @endforeach
			</p>
			<hr>
			
		</div>
	</div>
	
	<footer style="border-top: 1px solid #d2d6de; margin-top: 30px; padding: 20px 0;
    text-align: center;"> © 2016 Laravel. All rights reserved. 
	</footer>
    
  </body>
</html>

