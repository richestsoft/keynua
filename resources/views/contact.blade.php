@extends('layouts.app')

@section('htmlheader_title')
	Contact Us
@endsection

@section('content')
   <div class="contact">
        <div class="container">
            <h1 class="head-main">Contact <span>Us</span></h1>
            <div class="contact_inner">
                <div class="row">
                    <div class="col-sm-6 contact_left">
                        <h2 class="contact_head">Key<span>nua</span></h2>
                        <p>Are you looking for custom magnets...FAST? You've come to the right place. We're a custom magnet supplier that produces custom refrigerator magnets like Business Card Magnets, Save the Date Magnets &amp; Calendar Magnets.</p>
                        <div class="row">
                            <div class="col-sm-6 contact_info">
                                <h4>Address</h4>
                                <ul>
                                   <li>345 Industrial Ln</li>
                                   <li>Birmingham, AL 35211</li>
                                </ul>
                            </div>
                            <div class="col-sm-6 contact_info">
                                <h4>Email address</h4>
                                <ul>
                                    <li>General : carl@keynua.com</li>
                                    <li>Office :  dale@keynua.com</li>
                                </ul>
                            </div>
                            <div class="col-sm-6 contact_info">
                                <h4>Phone</h4>
                                <ul>
                                    <li>Landline : (205) 945.8444</li>
                                    <li>Fax :  (205) 942.9060</li>
                                </ul>
                            </div>
                            <div class="col-sm-6 contact_info social_contact">
                                <h4>SOcial Media</h4>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 contact_form">
                        <h2 class="contact_head">COntact <span>Form</span></h2>
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/contactus_query') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="text" name="name" placeholder="Name*">
                            <input type="email" name="email" placeholder="Email*">
                            <input type="text" name="subject" placeholder="Subject*">
                            <textarea name="comment" placeholder="Comment*"></textarea>
                            <input type="submit" value="submit" name="">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contat_map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3328.9820193768164!2d-86.85570168546361!3d33.44977498077411!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88891efb67e8de8f%3A0x6dc07cdb255b233c!2sArt+Bindery!5e0!3m2!1sen!2sin!4v1485522059753" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
@endsection
