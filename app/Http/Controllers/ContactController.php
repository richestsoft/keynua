<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact');
    }
	public function submitQuery(Request $request)
	{
		$this->validate($request, [			
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'subject' => 'required',
			'comment' => 'required',
		]);
		/*  -------------------------- Conatact Us Email --------------------------- */
		if($request)
		{
			$email = $request['email'];
			//$comment = $request['comment'];
			$name = $request['name'];
			$subject= $request['subject'];
			
			$data = $request->only('name', 'email', 'subject','comment');
			$data['messageLines'] = explode("\n", $request->get('comment'));

			\Mail::send('emails.contactusNotification', $data, function ($message) use ($data) {
			  $message->subject('ContactUs User: '.$data['name'])
					  ->to('info@.com')
					  ->from('info@.com', 'Keynua');
			});
			
		}
		/*  -------------------------- End for Conatact Us Email -------------------- */
	}
}
